package de.matoatoa.interceptor.starter

import org.springframework.stereotype.Component
import org.springframework.web.context.annotation.RequestScope
import java.util.*

@RequestScope
@Component
data class RequestContext(var id: String = UUID.randomUUID().toString().dropLast(28))