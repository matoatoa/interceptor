package de.matoatoa.interceptor.starter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RequestFilter : OncePerRequestFilter() {

    @Autowired
    lateinit var requestContext: RequestContext

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) =
            logger.info("Filter:     ${requestContext.id}").apply {
                filterChain.doFilter(request, response)
            }


}