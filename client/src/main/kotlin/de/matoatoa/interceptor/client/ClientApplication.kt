package de.matoatoa.interceptor.client

import de.matoatoa.interceptor.starter.RequestContext
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@RestController
class ClientApplication{

    private val logger = LoggerFactory.getLogger(ClientApplication::class.java)

    @Autowired
    lateinit var requestContext : RequestContext

    @GetMapping
    fun hallo () = logger.info("Controller: ${requestContext.id}")

}

fun main(args: Array<String>) {
    SpringApplication.run(ClientApplication::class.java, *args)
}
